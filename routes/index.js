var express = require('express');
var router = express.Router();

const category=require('./category')
const product=require('./product')

router.use('/category',category);
router.use('/product',product);


module.exports = router;
