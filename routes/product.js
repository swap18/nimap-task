var express = require('express');
var router = express.Router();



const {addProduct,get_all_products,deleteProduct,updateProduct} = require('../controller/product');

router.get('/', get_all_products);
router.post('/', addProduct);
router.delete('/', deleteProduct);
router.patch('/', updateProduct);





module.exports = router;