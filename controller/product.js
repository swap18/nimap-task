const Product = require('../model/product')
const Category = require('../model/category')

exports.addProduct = async (req, res, next) => {
    
    const{name,categoryId}=req.body
    let category = await Category.findById(categoryId)
    if(!category){
        return res.status(404).json({message:'Invalid Category'})
    }
    let product = await Product.create({name,categoryId})
    return res.status(200).json({ message: 'Product Added', data: [{ product }] })
}

exports.get_all_products = async (req, res, next) => {
    const { page = 1, limit = 10 } = req.query;
    let product = await Product.find().populate(
        {
            path: 'categoryId'
        }
    ).limit(limit * 1).skip((page - 1) * limit);
    return res.status(200).json({ message: 'All Products fetched', data: [{ product }] })
}



exports.deleteProduct = async (req, res, next) => {


    const { id } = req.query
    let product = await Product.findByIdAndRemove(id)
    return res.status(200).json({ message: 'Product Deleted', data: [{ product }] })
}



exports.updateProduct = async (req, res, next) => {
    
    const { name,categoryId } = req.body
    const {id}= req.query
    let category = await Category.findById(categoryId)
    if(!category){
        return res.status(404).json({message:'Invalid Category'})
    }
    let product = await Product.findByIdAndUpdate(id,{$set:{name,categoryId}},{new:true})
    
    return res.status(200).json({ message: 'Update success', UpdatedUser: [{ product }] })
}